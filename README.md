# lichthagel.gitlab.io

[![pipeline status](https://gitlab.com/Lichthagel/lichthagel.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/Lichthagel/lichthagel.gitlab.io/commits/master)
[![Discord](https://img.shields.io/discord/148103700743847936.svg)](https://discordapp.com/invite/0pI32FvBW7M0f6A6)
[![Twitter Follow](https://img.shields.io/twitter/follow/Lichthagel.svg?label=Follow&style=social)](https://twitter.com/Lichthagel)

## Used stuff

- [Node](https://nodejs.org/en/)
- [Nunito](https://fonts.google.com/specimen/Nunito)
- [Font Awesome](https://fontawesome.com/)
- [gulp](https://gulpjs.com/)
- [gulp-clean](https://www.npmjs.com/package/gulp-clean)
- [gulp-clean-css](https://www.npmjs.com/package/gulp-clean-css)
- [gulp-ejs](https://www.npmjs.com/package/gulp-ejs)
- [gulp-htmlmin](https://www.npmjs.com/package/gulp-htmlmin)
- [gulp-rename](https://www.npmjs.com/package/gulp-rename)
- [gulp-sitemap](https://www.npmjs.com/package/gulp-sitemap)
- [merge-stream](https://www.npmjs.com/package/merge-stream)