const { src, dest, parallel } = require("gulp");
const htmlmin = require("gulp-htmlmin");
const connect = require("gulp-connect");

function buildSVG() {
  return src("src/img/*.svg")
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(dest("public/img"))
    .pipe(connect.reload());
}

function copyImages() {
  return src(["src/img/*", "!src/img/*.svg"])
    .pipe(dest("public/img"))
    .pipe(connect.reload());
}

exports.buildImages = parallel(buildSVG, copyImages);
