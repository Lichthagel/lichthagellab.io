const { src, dest } = require("gulp");
const cleanCSS = require("gulp-clean-css");
const connect = require("gulp-connect");

function buildCSS() {
  return src("src/css/*.css")
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(dest("public/css"))
    .pipe(connect.reload());
}

exports.buildCSS = buildCSS;
