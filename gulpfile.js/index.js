const { parallel, series, src, watch } = require("gulp");
const { buildCSS } = require("./build_css");
const { buildHTML } = require("./build_html");
const { buildImages } = require("./build_images");
const { buildSitemap } = require("./build_sitemap");
const gulpClean = require("gulp-clean");
const connect = require("gulp-connect");

exports.clean = function () {
  return src("public").pipe(gulpClean());
};
exports.watch = function () {
  watch("src/css/*.css", buildCSS);
  watch("src/img/*", buildImages);
  watch(
    ["src/templates/index.ejs", "src/templates/redirect.ejs", "src/links.json"],
    buildHTML
  );
};
exports.serve = function (cb) {
  connect.server({
    root: "public",
    livereload: true,
  });
  exports.watch();
  cb();
};
exports.default = parallel(
  series(buildHTML, buildSitemap),
  buildCSS,
  buildImages
);
