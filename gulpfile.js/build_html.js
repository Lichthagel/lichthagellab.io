const { parallel, src, dest } = require("gulp");
const ejs = require("gulp-ejs");
const htmlmin = require("gulp-htmlmin");
const rename = require("gulp-rename");
const mergeStream = require("merge-stream");
const fs = require("fs");
const connect = require("gulp-connect");

function buildHome() {
  let links = JSON.parse(fs.readFileSync("src/links.json"));
  let linked = links.filter((x) => x.linked);

  return src("src/templates/index.ejs")
    .pipe(
      ejs({
        links: linked,
        images: 9,
      })
    )
    .pipe(rename({ extname: ".html" }))
    .pipe(htmlmin({ collapseWhitespace: true, minifyJS: true }))
    .pipe(dest("public"))
    .pipe(connect.reload());
}

function buildRedirects() {
  let links = JSON.parse(fs.readFileSync("src/links.json"));

  return mergeStream(
    links.map(function (link) {
      return src("src/templates/redirect.ejs")
        .pipe(
          ejs({
            link: link,
          })
        )
        .pipe(rename({ extname: ".html", basename: "index" }))
        .pipe(htmlmin({ collapseWhitespace: true, minifyJS: true }))
        .pipe(dest("public/" + link.id));
    })
  )
  .pipe(connect.reload());
}

exports.buildHTML = parallel(buildHome, buildRedirects);
