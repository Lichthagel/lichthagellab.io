const { src, dest, parallel } = require("gulp");
const sitemap = require("gulp-sitemap");

function buildSitemap() {
  return src("public/**/*.html")
    .pipe(
      sitemap({
        siteUrl: "https://lichthagel.de",
      })
    )
    .pipe(dest("public"));
}

function copyRobotsTxt() {
  return src("src/robots.txt").pipe(dest("public"));
}

exports.buildSitemap = parallel(buildSitemap, copyRobotsTxt);
